# Changelog

#### 1.1.1

- Chore: accept `base` `3.0`.

### 1.1.0

- Feature: add `toStringWithCustomVersionHeader` to transform an `:: XMLDoc` to an XML `String`
           with a custom XML version header.
## 1.0.0

- Initial version.
