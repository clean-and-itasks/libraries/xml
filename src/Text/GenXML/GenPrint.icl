implementation module Text.GenXML.GenPrint

import Data.Maybe
import Text.GenXML, Text.GenPrint

derive gPrint XMLDoc, XMLQName, XMLNode, XMLAttr
